import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from '../../../services/heroes.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
})
export class DetailComponent implements OnInit {

  heroe: any = {};
  casa: string = "";

  constructor(private pmRout: ActivatedRoute,
              private http: HeroesService) {
    this.pmRout.params.subscribe(p => {
      this.heroe = this.http.getHeroe(p['id']);
    })
  }

  ngOnInit() {
  }

  logo(casaHeroe: string){
    if (casaHeroe == "Marvel") {
      this.casa = "assets/img/Marvel.png"
      return this.casa;
    }else{
      this.casa = "assets/img/DC.png"
      return this.casa;
    }
  }

}
