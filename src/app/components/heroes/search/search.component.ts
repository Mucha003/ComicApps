import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from '../../../services/heroes.service';
import { Heroe } from '../../../interfaces/heroe';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit {

  ListHeroes: Heroe[] = [];
  search:string;

  constructor(private pmRout: ActivatedRoute,
              private http: HeroesService) { }

  ngOnInit() {
    this.FindHeroe();
  }

  FindHeroe() {
    this.pmRout.params.subscribe(p => {
      this.search = p['name'];
      this.ListHeroes = this.http.findHeroe(p['name']);
    })
  }
}
