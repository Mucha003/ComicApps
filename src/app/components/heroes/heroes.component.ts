import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../../services/heroes.service'
import { Heroe } from '../../interfaces/heroe';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  ListHeroes: Heroe[] = [];

  constructor(private http: HeroesService,
              private rout:Router)
              { }

  ngOnInit() {
    this.ListHeroes = this.http.getAllHeroes();
  }

  HeroeDetail(id:number){
    this.rout.navigate(['/detail', id])
  }

}
