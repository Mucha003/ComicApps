import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
})
export class CardComponent implements OnInit {

  //cette prop peut venir depui l'exterieur
  @Input() heroe: any = {};
  @Input() Index: number;

  @Output() heroesSelect: EventEmitter<number>;

  constructor(private rout:Router) {
    this.heroesSelect = new EventEmitter();
  }

  ngOnInit() {
  }

  HeroeDetail(){
    this.rout.navigate(['/detail', this.Index]);
    // this.heroesSelect.emit(this.Index);
  }

}
