import { RouterModule, Routes } from '@angular/router';
import { Component } from '@angular/core';

import { HomeComponent } from './components/home/home.component'
import { AboutComponent } from './components/about/about.component'
import { HeroesComponent } from './components/heroes/heroes.component'
import { DetailComponent } from './components/heroes/detail/detail.component'
import { SearchComponent } from './components/heroes/search/search.component'

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'heroes', component: HeroesComponent },
  { path: 'search/:name', component: SearchComponent },
  { path: 'detail/:id', component: DetailComponent },
  { path: '**',pathMatch: 'full',redirectTo: 'home' },
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
// pour le # dans la url
// export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash:true });
